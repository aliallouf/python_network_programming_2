import socket

# Connect to the server
host = '127.0.0.1'
port = 5000
client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect((host, port))

# Receive quiz questions and send answers
while True:
    question = client.recv(1024).decode()
    if not question:
        break
    answer = input(question)
    client.sendall(answer.encode())


score = client.recv(1024).decode()
print(score)

client.close()