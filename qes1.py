import json
import socket
import threading
with open('qes.json', 'r') as f:
    d = json.load(f)

# Function to handle client connections
def handle_client_connection(csock, addr):
    print(f"New client connected: {addr}")
    score = 0
    for question in d["questions"]:
        csock.sendall(question["question"].encode())
        client_answer = csock.recv(1024).decode().strip()
        if client_answer.lower() == question["answer"].lower():
            score += 1
    csock.sendall(f"Your final score is: {score}".encode())
    csock.close()

# Start the server
host = '127.0.0.1'
port = 5000
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind((host, port))
server.listen(10)

print(f"Server listening on {host}:{port}")

# Handle client connections
while True:
    csock, addr = server.accept()
    threading.Thread(target=handle_client_connection, args=(csock, addr)).start()