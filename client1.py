import socket

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect(('127.0.0.1', 5000))

# Receive quiz questions and send answers
while True:
    question = client.recv(1024).decode()
    if not question:
        break
    answer = input(question)
    client.sendall(answer.encode())

# Receive final score
score = client.recv(1024).decode()
print(score)

# Close the connection
client.close()